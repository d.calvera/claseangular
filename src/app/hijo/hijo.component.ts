import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})
export class HijoComponent implements OnInit {

  constructor() { }

  @Input() recibido: Array<number> = new Array;

  @Output() enviarPapa = new EventEmitter<string>();

  ngOnInit(): void {
    console.log(this.recibido);
  }

 public enviar(){
   let respuesta = this.recibido[0] + this.recibido[1];
   console.log(respuesta);
   this.enviarPapa.emit(String(respuesta));
 }

}
