import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public nombres: string[] = ["Carlos" , "Juan", "Ana"];
  public nombres2: string[] = ["Camilo", "hugo"];



  public propertyBinding : string = "Mensaje";

  public activedButton: boolean = false;

  public imagenUrl: string = "https://placeimg.com/400/200/any"

  constructor(){

    

  }

  public mostrarEvento(event: any):void{
    console.log('enter fue presionado.');
  }




  public desactivarBoton(data:boolean):void {

    this.activedButton = data;
  }
  public addFirstElement(name: string) : void {
    this.nombres.unshift(name);
    
  }

  public addLastElement(name: string) : void {
    this.nombres.push(name);
  }

  public deleteFirstElement() : void {
    let respuesta = this.nombres.shift();
    console.log("Se eliminó el valor de " + respuesta);
  }

  public deleteLastElement() : void {
    let respuesta = this.nombres.pop();
    console.log("Se eleminó el ultimo elemento " + respuesta);
  }

  public concatenar(array: string[]): string[] {
    return this.nombres.concat(array);
  }

}
